from megapi import *

import os
from os import path

import time
import bottle
from bottle import get, request, template, route, static_file
from bottle import SimpleTemplate
from bottle_utils.csrf import csrf_token
bottle.debug(True)

# import time
# while True:
#     for x in range(-100,100):
#         if (x < 0):
#             print((x + 100) * " " + "-->")
#         else:
#             print((100 - x) * " " + "<--")
#         time.sleep(0.1)

status_Port1 = None
status_Port2 = None
bot = MegaPi()

def test_fd_beide():
    global bot
    global status_Port1
    global status_Port2
    status_two = 0                                          # Vorberreitung
    status = 0
    status_Port1 = 0
    status_Port2 = 0

    bot.encoderMotorMove(1, 200, 1000, None)
    status_Port1 += 1                                       #Port1 wird gestartet + status_Port1 bekommt + 1
    bot.encoderMotorMove(2, 200, 1000, None)
    status_Port2 += 1                                       #Port2 wird gestartet + status_Port2 bekommt + 1

    bot.encoderMotorMove(1, 0, 0, None)
    status_Port1 += 1                                       #Port1 wird aus gemacht + status_Port1 bekommt + 1
    bot.encoderMotorMove(2, 0, 0, None)
    status_Port2 += 1                                       #Port2 wird aus gemacht + status_Port2 bekommt + 1
    time.sleep(4)

    bot.encoderMotorSetCurPosZero(1)
    status_Port1 += 1                                       #Port1 wird auf Null gesetzt + status_Port1 + 1
    bot.encoderMotorSetCurPosZero(1)
    status_Port2 += 1                                       #Port2 wird auf Null gesetzt + status_Port2 + 1

def botBereiten(func):
    def inni(*args, **kwargs):
        global bot

        bot.start()
        func(*args, **kwargs)
        bot.close()
    return inni

@botBereiten
def botMove(slot, speed, distance):
    bot.encoderMotorMove(slot, speed, distance, None)

@botBereiten
def botGrip(closeGrip):
    if closeGrip:
        bot.motorMove(100, 0)
    else:
        bot.motorMove(0, 100)

@get('/')
@csrf_token
def getIndex():
    """Zeigt die Index Seite an"""
    return template('index', token=request.csrf_token)

@get("/api/left90")
def setLeft90():
    botMove(1, 500, 2000)
    botMove(2, 500, 2000)
    return "<div>{message:'left90'}</div>"

@get("/api/left45")
def setLeft45():
    botMove(1, 500, 1000)
    botMove(2, 500, 1000)
    return "<div>{message:'left45'}</div>"

@get("/api/right90")
def setRight90():
    botMove(1, 500, -2000)
    botMove(2, 500, -2000)
    return "<div>{message:'right90'}</div>"

@get("/api/right45")
def setRight45():
    botMove(1, 500, -1000)
    botMove(2, 500, -1000)
    return "<div>{message:'right45'}</div>"

@get("/api/forward")
def setForward():
    botMove(1, 500, -1000)
    botMove(2, 500, 1000)
    return "<div>{message:'forward'}</div>"

@get("/api/fastForward")
def setFastForward():
    botMove(1, 1000, -1000)
    botMove(2, 1000, 1000)
    return f"<div>Port1 ={status_Port1}, Port2 ={status_Port2}</div>"

@get("/api/backward")
def setBackward():
    botMove(1, 500, 1000)
    botMove(2, 500, -1000)
    return "<div>{message:'backward'}</div>"

@get("/api/fastBackward")
def setFastBackward():
    botMove(1, 1000, 1000)
    botMove(2, 1000, -1000)
    return "<div>{message:'fastBackward}</div>"

@get("/api/down")
def setDown():
    botMove(3, 100, 300)
    return "<div>{message:'down'}</div>"

@get("/api/take")
def setFast():
    botGrip(True)
    return "<div>{message:'take'}</div>"

@get("/api/drop")
def setDrop():
    botGrip(False)
    return "<div>{message:'drop'}</div>"

@get("/api/up")
def setUp():
    botMove(3, 100, -300)
    return "<div>{message:'up'}</div>"


@route('/img/<filename:path>', name='img')
def getImage(filename):
    return static_file(filename, root='img')

BASE_DIR = path.abspath('.')

app = bottle.default_app()
app.config.load_config(os.path.join(BASE_DIR, 'settings.ini'))

bottle.TEMPLATE_PATH = [ os.path.join(BASE_DIR) ]
SimpleTemplate.defaults = app.config.get('html', {})
SimpleTemplate.defaults['url'] = bottle.url
SimpleTemplate.defaults['page_title'] = app.config.get('html.page_title')
SimpleTemplate.defaults['robot_image'] = app.config.get('html.robot_image')


if __name__ == '__main__':
    app.run()
# Run bottle in application mode (in production under uWSGI server).
else:
    application = app
